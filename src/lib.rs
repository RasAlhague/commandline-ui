pub mod label_control;

pub use crate::label_control::LabelControl;

pub trait UiElement {
    fn name(&self) -> String;
    fn display(&self);
}

pub trait UiApp<'a> {
    fn initialise_elements(&mut self);
    fn get_element(&self, key: &str) -> Option<&Box<dyn UiElement>>;
    fn add_element(&mut self, key: &'a str, ui_element: Box<dyn UiElement>);
    fn remove_element(&mut self, key: &str);
    fn set_startup_element(&mut self, name: &str);
    fn run(&self) -> Result<(), &'static str>;
}